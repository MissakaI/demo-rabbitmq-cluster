package demo.rabbitmq;

import java.util.HashMap;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.GetResponse;

/**
 * Consumer
 */
public class ConsumerAckHashExchange {

    // private static final String MQ_HOST = "rabbitmq-headless.default.svc.cluster.local";
    private static final String MQ_HOST = "localhost";
    private static final String QUEUE_NAME = "ha-demo.queue";
    private static final HashMap<String,Object> QUEUE_ARGS = new HashMap<>();
    private static final String EXCHANGE_NAME = "hash1";
    private static final String CONSISTENT_HASH_EXCHANGE_TYPE = "x-consistent-hash";

    public static void main(String[] args) {
        initQueueArgs();

        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost(MQ_HOST);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setUsername("user");
        factory.setPassword("3YeL84?u");

        try (Connection connection=factory.newConnection()) {
            Channel channel=connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, CONSISTENT_HASH_EXCHANGE_TYPE, true, false, null);
            channel.basicQos(10, true);
            String queue=channel.queueDeclare().getQueue();

            System.out.println(" [*] Waiting for messages on queue: "+queue+"\nPress Ctrl+C to Exit.");

            // DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            //     String message = new String(delivery.getBody(), "UTF-8");
            //     System.out.println(" [x] Received '" + message + "'");
            //     try{
            //         Thread.sleep(500);
            //         channel.basicAck(delivery.getEnvelope().getDeliveryTag(), true);
            //     }catch(InterruptedException e){
            //         System.err.println("[ERROR] : "+e.getMessage());
            //         channel.basicNack(delivery.getEnvelope().getDeliveryTag(), true, true);
            //     }
            // };

            // while(true){
            //     channel.basicConsume(QUEUE_NAME, false, deliverCallback, consumerTag -> { });
            // }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final void initQueueArgs(){
        // QUEUE_ARGS.put("x-queue-type", "quorum");
    }

}
