package demo.rabbitmq;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

/**
 * Consumer
 */
public class ConsumerWriteToFile {

    private static final String MQ_HOST = "localhost";
    private static final String QUEUE_NAME = "ha-demo.queue";
    private static final HashMap<String, Object> QUEUE_ARGS = new HashMap<>();

    public static void main(String[] args) {
        initQueueArgs();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(MQ_HOST);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setUsername("user");
        factory.setPassword("3YeL84?u");

        try (Connection connection = factory.newConnection()) {
            Channel channel = connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, true, false, false, QUEUE_ARGS);

            setOutput(args);

            System.out.println(" [*] Waiting for messages. Press Ctrl+C to Exit.");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    System.err.println("[ERROR] : " + e.getMessage());
                }
            };

            channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {});

            while (true);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.flush();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.flush();
        }
    }

    private static final void initQueueArgs() {
        // QUEUE_ARGS.put("x-queue-type", "quorum");
    }

    private static void setOutput(String[] args) {
        String consumer = "";
        if (args.length > 0) {
            consumer = args[0];
        }else{
            consumer=LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HHmmss"));
        }

        File outFile = new File("/app/out/consumer_" + consumer + ".txt");
        outFile.getParentFile().mkdirs();
        // outFile.createNewFile();

        try {
            System.setOut(new PrintStream(outFile));
            System.setErr(new PrintStream(new File("/app/out/consumer_err_" + consumer + ".txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
