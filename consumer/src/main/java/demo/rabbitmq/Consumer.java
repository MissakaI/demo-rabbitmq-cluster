package demo.rabbitmq;

import java.util.HashMap;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

/**
 * Consumer
 */
public class Consumer {

    private static final String MQ_HOST = "localhost";
    private static final String QUEUE_NAME = "ha-demo.queue";
    private static final HashMap<String,Object> QUEUE_ARGS = new HashMap<>();

    public static void main(String[] args) {
        initQueueArgs();

        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost(MQ_HOST);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setUsername("user");
        factory.setPassword("3YeL84?u");

        try (Connection connection=factory.newConnection()) {
            Channel channel=connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, true, false, false, QUEUE_ARGS);

            System.out.println(" [*] Waiting for messages.");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                System.out.println(" [x] Received '" + message + "'");
            };
            
            channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });

            Thread.sleep(1000*30);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final void initQueueArgs(){
        // QUEUE_ARGS.put("x-queue-type", "quorum");
    }

}
