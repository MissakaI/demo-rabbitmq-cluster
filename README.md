# RabbitMQ-Demo

## Helm installation

1. Add your custom configuration to `bitnami-rabbitmq-config.yaml`

> Note: To generate custom password and erlang cookie check the [Optional Configurations](#optional-configurations) section.

2. To install helm chart
        
       helm install demo-rabbitmq -f .\bitnami-rabbitmq-config.yaml bitnami/rabbitmq
    
3. To access the pods, you need to use `port-forwarding` on `kubectl`

       kubectl port-forward --namespace default svc/demo-rabbitmq 5672:5672 15672:15672

    `5672` and `5671` are AMQP access ports
    
    `15672` is RabbitMQ Management Interface port

## Scaling

We can scale using `helm upgrade` or `kubectl scale`

### Helm upgrade

1. To upgrade this configuration use

       helm upgrade -f .\bitnami-rabbitmq-config.yaml --set replicaCount=5 demo-rabbitmq bitnami/rabbitmq

   Note: Use the following steps if you didn't use the pre-configured `yaml` with usernames and passwords and erlang cookies 

2. Get the generated password

   #### PowerShell

       $pass = kubectl get secret --namespace default demo-rabbitmq -o jsonpath="{.data.rabbitmq-password}"
       $pass = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($pass))

   #### Bash

       $pass = kubectl get secret --namespace default demo-rabbitmq -o jsonpath="{.data.rabbitmq-password}"  | base64 --decode

3. Get the generated `erlang cookie`

    #### PowerShell

       $erlCookie = kubectl get secret --namespace default demo-rabbitmq -o jsonpath="{.data.rabbitmq-erlang-cookie}"
       $erlCookie = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($elCookie))

   #### Bash

       $erlCookie = kubectl get secret --namespace default demo-rabbitmq -o jsonpath="{.data.rabbitmq-erlang-cookie}"  | base64 --decode

4. Set replica count

       $replicaCount=5

5. To scale up using helm
       
       helm upgrade \
       --set replicaCount=$replicaCount \
       --set auth.password=$pass \
       --set auth.erlangCookie=$erlCookie \
       demo-rabbitmq bitnami/rabbitmq

#### Additional Operations

> Note: Following steps are only required if you scale down permenantly and has no intention of scaling up recently.

1. Detach pods that are shutdown

       kubectl exec demo-rabbitmq-0 --container rabbitmq -- rabbitmqctl forget_cluster_node rabbit@demo-rabbitmq-3.demo-rabbitmq-headless.default.svc.cluster.local

2. Check logs for proper detachment and wait until detach

3. After successful detachment remove PersistenceVolumeClaim

       kubectl delete pvc data-demo-rabbitmq-3

## Mirrored Queues

Add a RabbitMQ Policy to initiate Mirrored Queues. Use following command to set it.

#### Bash

       kubectl exec demo-rabbitmq-0 --container rabbitmq \
       -- rabbitmqctl set_policy ha-demo "^ha-demo\." \
       '{"ha-mode":"exactly","ha-params":3}'

#### PowerShell

       kubectl exec demo-rabbitmq-0 --container rabbitmq -it -- /bash/sh
       rabbitmqctl set_policy ha-demo "^ha-demo\." '{"ha-mode":"exactly","ha-params":3}'

The above command will mark all Queues starting with name prefix `ha-demo` as High Avaliability with exactly 3 replicas.
For more options view the [documentation on mirrored queues](https://www.rabbitmq.com/ha.html)

## Optional Configurations

### PS Script to generate random password and erlang cookie.

       $erlCookie = -join ( (48..57) + (65..90) + (97..122) + (33,42,43,58,61,63,64) + (35..38) | Get-Random -Count 32 | % {[char]$_})

       $password = -join ( (48..57) + (65..90) + (97..122) + (33,42,43,58,61,63,64) + (35..38) | Get-Random -Count 10 | % {[char]$_})


<!-- ## Docker Installation

1. Create Docker Network

        docker network create rabbits

2. Run docker image

        docker run -it --rm --name rabbitmq --network  -p 5672:5672 -p 15672:15672 rabbitmq -->