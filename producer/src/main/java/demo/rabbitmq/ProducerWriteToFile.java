package demo.rabbitmq;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Producer
 */
public class ProducerWriteToFile {

    private static final String MQ_HOST = "localhost";
    private static final String QUEUE_NAME = "ha-demo.queue";
    private static final HashMap<String,Object> QUEUE_ARGS = new HashMap<>();

    public static void main(String[] args) {
        initQueueArgs();

        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost(MQ_HOST);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setUsername("user");
        factory.setPassword("3YeL84?u");

        try (Connection connection=factory.newConnection()) {
            Channel channel=connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, true, false, false, QUEUE_ARGS);

            setOutput(args);

            for (int i = 0; ; i++) {
                String message="Hello RabbitMQ "+i;
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");
                Thread.sleep(300);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final void initQueueArgs(){
        // QUEUE_ARGS.put("x-queue-type", "quorum");
    }

    private static void setOutput(String[] args) {
        String consumer = "";
        if (args.length > 0) {
            consumer = args[0];
        }else{
            consumer=LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HHmmss"));
        }

        File outFile = new File("/app/out/producer_" + consumer + ".txt");
        outFile.getParentFile().mkdirs();
        // outFile.createNewFile();

        try {
            System.setOut(new PrintStream(outFile));
            System.setErr(new PrintStream(new File("/app/out/producer_err_" + consumer + ".txt")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
