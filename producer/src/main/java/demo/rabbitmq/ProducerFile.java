package demo.rabbitmq;

import java.io.InputStream;
import java.util.HashMap;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Producer
 */
public class ProducerFile {

    private static final String MQ_HOST = "localhost";
    private static final String QUEUE_NAME = "ha-demo.queue";
    private static final HashMap<String,Object> QUEUE_ARGS = new HashMap<>();

    public static void main(String[] args) {
        initQueueArgs();

        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost(MQ_HOST);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setUsername("user");
        factory.setPassword("3YeL84?u");

        try (Connection connection=factory.newConnection()) {
            Channel channel=connection.createChannel();
            channel.queueDeclare(QUEUE_NAME, true, false, false, QUEUE_ARGS);

            InputStream stream = ProducerFile.class.getResourceAsStream("/wallpaper.jpg");
            byte[] byteStream = stream.readAllBytes();
            for (int i = 0; i < 50; i++) {
                channel.basicPublish("", QUEUE_NAME, null, byteStream);
                System.out.println(" [x] Sent File : "+(i+1));
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final void initQueueArgs(){
        // QUEUE_ARGS.put("x-queue-type", "quorum");
    }

}
