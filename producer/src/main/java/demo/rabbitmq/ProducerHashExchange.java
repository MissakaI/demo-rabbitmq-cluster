package demo.rabbitmq;

import java.util.HashMap;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * Producer
 */
public class ProducerHashExchange {

    private static final String MQ_HOST = "localhost";
    private static final String QUEUE_NAMES[] = {"q1","q2","q3"};
    private static final String EXCHANGE_NAME = "hash1";
    private static final HashMap<String,Object> QUEUE_ARGS = new HashMap<>();
    private static final String CONSISTENT_HASH_EXCHANGE_TYPE = "x-consistent-hash";

    public static void main(String[] args) {
        initQueueArgs();

        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost(MQ_HOST);
        factory.setAutomaticRecoveryEnabled(true);
        factory.setUsername("user");
        factory.setPassword("3YeL84?u");

        try (Connection connection=factory.newConnection()) {
            Channel channel=connection.createChannel();
            channel.exchangeDeclare(EXCHANGE_NAME, CONSISTENT_HASH_EXCHANGE_TYPE, true, false, null);

            for (String queue: QUEUE_NAMES){
                channel.queueDeclare(queue, true, false, false, QUEUE_ARGS);
                channel.queuePurge(queue);
                channel.queueBind(queue, EXCHANGE_NAME, "1"); // 1 in the end declares the weight. here all queues given same weight
            }
            

            for (int i = 0; i<100; i++) {
                String message="Hello RabbitMQ "+i;
                channel.basicPublish(EXCHANGE_NAME, String.valueOf(i), null, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");
                Thread.sleep(100);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final void initQueueArgs(){
        // QUEUE_ARGS.put("x-queue-type", "quorum");
    }

}
